﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class which was intended to manage the events happening in a certain room/labyrinth cell.
/// As it turned out this is actually my GameManager class now. :)
/// </summary>
public class RoomManager : MonoBehaviour
{
    #region Fields

    private Dictionary<Orientation, GameObject> _walls;
    private Dictionary<Orientation, GameObject> _floors;
    private Dictionary<Position, SkeletonScript> _skeletons;

    private List<LabyrinthCell> _maze;
    private LabyrinthCell _currentRoom;
    private System.Random _random;

    #endregion Fields

    #region Singleton

    public static RoomManager Instance { get; set; }

    #endregion Singleton

    #region Properties

    public int MazeHeight = 32;
    public int MazeWidth = 32;
    public float ChanceForMonster = 0.1f;
    public float ChanceForLoot = 0.075f;
    public int RandomSeed = 0;

    public Button btn_Restart;
    public Button btn_Exit;
    public AudioSource Pickup;
    public AudioSource HitSkeleton;
    public GameObject[] Walls;
    public GameObject[] Floors;
    public GameObject Treasure;
    private int _currentMonsters;
    private bool _disableAudio;

    #endregion Properties

    #region Constructor

    public RoomManager()
    {
        _walls = new Dictionary<Orientation, GameObject>();
        _floors = new Dictionary<Orientation, GameObject>();
        _maze = new List<LabyrinthCell>();
        _skeletons = new Dictionary<Position, SkeletonScript>();
        Instance = this;
    }

    #endregion Constructor

    #region MonoBehaviour

    // Use this for initialization
    private void Start()
    {
        FillDictionaries();

        Restart();
    }

    private void FillDictionaries()
    {
        _walls.Add(Orientation.LEFT, Walls[0]);
        _walls.Add(Orientation.FRONT, Walls[1]);
        _walls.Add(Orientation.RIGHT, Walls[2]);
        _floors.Add(Orientation.LEFT, Floors[0]);
        _floors.Add(Orientation.FRONT, Floors[1]);
        _floors.Add(Orientation.RIGHT, Floors[2]);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    #endregion MonoBehaviour

    #region Public Methods

    #region Restart/Quit

    /// <summary>
    /// Method to Restart game. Handles procedural Maze generation for now.
    /// The option to use the same random seed is implemented, but there is currently no user interface to enter the seed.
    /// </summary>
    public void Restart()
    {
        // Loose Focus on Button
        btn_Restart.navigation = new Navigation();
        if (RandomSeed != 0)
        {
            _random = new System.Random(RandomSeed);
        }
        else
        {
            _random = new System.Random();
        }

        GenerateMaze(MazeHeight, MazeWidth);
        LootManager.Instance.Reset();
        // Remove Skeletons from previous fight... by killing them.. :)
        _disableAudio = true;
        if (_skeletons.ContainsKey(Position.LEFT))
            _skeletons[Position.LEFT].Attack(5);
        if (_skeletons.ContainsKey(Position.RIGHT))
            _skeletons[Position.RIGHT].Attack(5);
        _disableAudio = false;
        // Reset Player (HP, Attack, etc)
        PlayerManager.Instance.ResetPlayer();

        SetCurrentRoom(Vector2.zero);
    }

    /// <summary>
    /// Shutdown the Application
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

    #endregion Restart/Quit

    #region Register Methods

    /// <summary>
    /// Registering and hiding the Wall Gameobjects/Sprites
    /// </summary>
    public void RegisterWall(GameObject wall, Orientation orientation)
    {
        wall.SetVisibility(Globals.ALPHA_HIDDEN);

        _walls.Add(orientation, wall);
    }

    /// <summary>
    /// Registering and hiding the Floor Gameobjects/Sprites
    /// </summary>
    public void RegisterFloor(GameObject floor, Orientation orientation)
    {
        floor.SetVisibility(Globals.ALPHA_VISIBLE);
        _floors.Add(orientation, floor);
    }

    /// <summary>
    /// Registering the Treasure Gameobjects/Sprites
    /// </summary>
    public void RegisterTreasure(GameObject gameObject)
    {
        Treasure = gameObject;
    }

    /// <summary>
    /// Registering the Skeletons/their respective scripts
    /// </summary>
    public void RegisterSkeleton(SkeletonScript skeleton)
    {
        _skeletons.Add(skeleton.Position, skeleton);
    }

    #endregion Register Methods

    #region Player Actions

    /// <summary>
    /// Player turning around -> therefore changing the walls of the room
    /// </summary>
    /// <param name="lookDirection"></param>
    public void TurnAround(Direction lookDirection)
    {
        if (_currentRoom != null)
            SetCurrentRoom(_currentRoom.Position, lookDirection);
        else
        {
            SetCurrentRoom(Vector2.zero, lookDirection);
        }
    }

    /// <summary>
    /// Player moving to adjacent room
    /// </summary>
    /// <param name="lookDirection"></param>
    public void MoveForward(Direction lookDirection)
    {
        if (CanLeave())
        {
            switch (lookDirection)
            {
                case Direction.NORTH:
                    if (_currentRoom.North != null)
                    {
                        SetCurrentRoom(_currentRoom.North.Position, lookDirection);
                    }
                    break;

                case Direction.EAST:
                    if (_currentRoom.East != null)
                    {
                        SetCurrentRoom(_currentRoom.East.Position, lookDirection);
                    }
                    break;

                case Direction.SOUTH:
                    if (_currentRoom.South != null)
                    {
                        SetCurrentRoom(_currentRoom.South.Position, lookDirection);
                    }
                    break;

                case Direction.WEST:
                    if (_currentRoom.West != null)
                    {
                        SetCurrentRoom(_currentRoom.West.Position, lookDirection);
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Player trying to attack skeleton in room (if available)
    /// </summary>
    /// <returns></returns>
    public Position AttackSkeleton(int dmg)
    {
        HitSkeleton.Play();
        if (_skeletons[Position.RIGHT].Attack(dmg))
            return Position.RIGHT; // could be combined with the default animation, just so stuff is more obvious...
        else if (_skeletons[Position.LEFT].Attack(dmg))
            return Position.LEFT;
        else
            return Position.RIGHT; // the right weapon swing will be shown even if the player can't hit something...
    }

    /// <summary>
    /// Player using his secondary action inside the room (NYI)
    /// </summary>
    /// <returns>Returns true if there was sth. to interact</returns>
    public bool UseSecondaryAction()
    {
        // no other use for a secondary action in this version
        return false;
    }

    #endregion Player Actions

    public void MonsterDeath()
    {
        _currentMonsters--;
        if (_currentMonsters == 0)
        {
            _currentRoom.HasMonster = false;
            TryToLoot();
        }
    }

    #endregion Public Methods

    #region Private Methods

    #region Maze Generation

    private void RemoveWall(LabyrinthCell currentCell, LabyrinthCell randomNeighbour)
    {
        if (currentCell.Position.x != randomNeighbour.Position.x)
        {
            if (currentCell.Position.x > randomNeighbour.Position.x)
            {
                currentCell.West = randomNeighbour;
                randomNeighbour.East = currentCell;
            }
            else
            {
                currentCell.East = randomNeighbour;
                randomNeighbour.West = currentCell;
            }
        }
        else
        {
            if (currentCell.Position.y > randomNeighbour.Position.y)
            {
                currentCell.South = randomNeighbour;
                randomNeighbour.North = currentCell;
            }
            else
            {
                currentCell.North = randomNeighbour;
                randomNeighbour.South = currentCell;
            }
        }
    }

    private List<LabyrinthCell> GetNeighbours(LabyrinthCell currentCell)
    {
        var result = new List<LabyrinthCell>();

        var north = _maze.FirstOrDefault(p => p.Position.x == currentCell.Position.x && p.Position.y == currentCell.Position.y + 1);
        var east = _maze.FirstOrDefault(p => p.Position.x == currentCell.Position.x + 1 && p.Position.y == currentCell.Position.y);
        var west = _maze.FirstOrDefault(p => p.Position.x == currentCell.Position.x - 1 && p.Position.y == currentCell.Position.y);
        var south = _maze.FirstOrDefault(p => p.Position.x == currentCell.Position.x && p.Position.y == currentCell.Position.y - 1);

        if (north != null)
        {
            result.Add(north);
        }

        if (east != null)
        {
            result.Add(east);
        }

        if (west != null)
        {
            result.Add(west);
        }

        if (south != null)
        {
            result.Add(south);
        }

        return result;
    }

    private void GenerateMaze(int height, int width)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                var cell = new LabyrinthCell()
                {
                    Position = new Vector2(x, y),
                };

                // No Treasure/Monsters in the starting room!
                if (x != 0 || y != 0)
                {
                    cell.HasMonster = _random.NextDouble() <= ChanceForMonster;
                    if (cell.HasMonster)
                    {
                        // Higher Chance for Treasure in Rooms with Monsters!
                        cell.HasTreasure = _random.NextDouble() * 3.0 <= ChanceForLoot;
                    }
                    else
                    {
                        cell.HasTreasure = _random.NextDouble() <= ChanceForLoot;
                    }
                }
                _maze.Add(cell);
            }
        }
        var currentCell = _maze.First();

        List<LabyrinthCell> stack = new List<LabyrinthCell>();
        while (_maze.Any(p => !p.IsVisited))
        {
            var neighbours = GetNeighbours(currentCell).Where(p => !p.IsVisited).ToArray();
            if (neighbours.Any())
            {
                var randomNeighbour = neighbours[_random.Next(0, neighbours.Length - 1)];
                stack.Add(currentCell);
                RemoveWall(currentCell, randomNeighbour);
                currentCell = randomNeighbour;
                currentCell.IsVisited = true;
            }
            else if (stack.Count > 0)
            {
                currentCell = stack.First();
                stack.Remove(currentCell);
            }
            else
            {
                break;
            }
        }

        SetCurrentRoom(Vector2.zero);
    }

    #endregion Maze Generation

    #region Set Properties for Current Room

    private void SetCurrentRoom(Vector2 position, Direction dir = Direction.NORTH)
    {
        _currentRoom = _maze.FirstOrDefault(room => room.Position.x == position.x && room.Position.y == position.y);

        SetWallVisibility(Orientation.FRONT, IsFrontWallVisible(dir));
        SetWallVisibility(Orientation.LEFT, IsLeftWallVisible(dir));
        SetWallVisibility(Orientation.RIGHT, IsRightWallVisible(dir));

        if (_currentRoom.HasMonster)
        {
            if (UnityEngine.Random.value <= 0.2f)
            {
                // 20% Chance for two Skeletons
                _skeletons[Position.LEFT].SetAlive();
                _skeletons[Position.RIGHT].SetAlive();
                _currentMonsters = 2;
            }
            else
            {
                // 80% Chance for one Skeleton
                _skeletons[Position.RIGHT].SetAlive();
                _currentMonsters = 1;
            }
        }

        if (_currentRoom.HasTreasure)
        {
            Treasure.SetVisibility(Globals.ALPHA_VISIBLE);
            TryToLoot();
        }
        else
        {
            Treasure.SetVisibility(Globals.ALPHA_HIDDEN);
        }
    }

    private void SetWallVisibility(Orientation orientation, bool visible)
    {
        if (visible)
        {
            _walls[orientation].SetVisibility(Globals.ALPHA_VISIBLE);
            _floors[orientation].SetVisibility(Globals.ALPHA_HIDDEN);
        }
        else
        {
            _walls[orientation].SetVisibility(Globals.ALPHA_HIDDEN);
            _floors[orientation].SetVisibility(Globals.ALPHA_VISIBLE);
        }
    }

    private bool IsFrontWallVisible(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return _currentRoom.North == null;

            case Direction.EAST:
                return _currentRoom.East == null;

            case Direction.SOUTH:
                return _currentRoom.South == null;

            case Direction.WEST:
                return _currentRoom.West == null;

            default:
                return true;
        }
    }

    private bool IsLeftWallVisible(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return _currentRoom.West == null;

            case Direction.EAST:
                return _currentRoom.North == null;

            case Direction.SOUTH:
                return _currentRoom.East == null;

            case Direction.WEST:
                return _currentRoom.South == null;

            default:
                return true;
        }
    }

    private bool IsRightWallVisible(Direction dir)
    {
        switch (dir)
        {
            case Direction.NORTH:
                return _currentRoom.East == null;

            case Direction.EAST:
                return _currentRoom.South == null;

            case Direction.SOUTH:
                return _currentRoom.West == null;

            case Direction.WEST:
                return _currentRoom.North == null;

            default:
                return true;
        }
    }

    private void TryToLoot()
    {
        if (_currentMonsters == 0 && _currentRoom.HasTreasure)
        {
            if (!_disableAudio)
            {
                Pickup.Play();
                _currentRoom.HasTreasure = false;
                if (_maze.Any(p => p.HasTreasure))
                {
                    PlayerManager.Instance.ItemReceived(LootManager.Instance.LootItem());
                }
                else
                {
                    LootManager.Instance.LootFinalItem();
                }
            }
        }
    }

    #endregion Set Properties for Current Room

    #region Checking the state of the Room

    private bool CanLeave()
    {
        // Player can only leave after Monster is defeated.
        return (_currentRoom != null);//&& !_currentRoom.HasMonster);
    }

    #endregion Checking the state of the Room

    #endregion Private Methods
}