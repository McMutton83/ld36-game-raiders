﻿using UnityEngine;

public class LootItem : MonoBehaviour
{
    #region Properties

    public float DropChance = 0;
    public LootType Type;

    #endregion Properties

    #region MonoBehaviour

    // Use this for initialization
    private void Start()
    {
        // Register Item with Chance
        LootManager.Instance.RegisterLoot(transform.gameObject, DropChance);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    #endregion MonoBehaviour
}