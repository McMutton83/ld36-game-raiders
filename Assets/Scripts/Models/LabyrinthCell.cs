﻿using UnityEngine;

public class LabyrinthCell
{
    public Vector2 Position { get; set; }
    public bool IsVisited { get; set; }
    public LabyrinthCell North { get; set; }
    public LabyrinthCell West { get; set; }
    public LabyrinthCell South { get; set; }
    public LabyrinthCell East { get; set; }
    public bool HasTreasure { get; set; }
    public bool HasMonster { get; set; }
}