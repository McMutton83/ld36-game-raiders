﻿using UnityEngine;

/// <summary>
/// Handling the behaviour of the skeletons
/// </summary>
public class SkeletonScript : MonoBehaviour
{
    #region Fields

    private int _currentHP = 0;
    private float _timeSinceLastAction = 0f;
    private bool _isAttacking;
    private float _timeToNextAction;

    #endregion Fields

    #region Properties

    public int InitialHP = 5;
    public GameObject ArmAttack;
    public GameObject ArmIdle;
    public Position Position;

    #endregion Properties

    #region MonoBehaviour

    // Use this for initialization
    private void Start()
    {
        ArmAttack.SetVisibility(Globals.ALPHA_HIDDEN);
        ArmIdle.SetVisibility(Globals.ALPHA_HIDDEN);
        transform.gameObject.SetVisibility(Globals.ALPHA_HIDDEN);

        RoomManager.Instance.RegisterSkeleton(this);
    }

    // Update is called once per frame
    private void Update()
    {
        if (_currentHP > 0)
        {
            _timeSinceLastAction += Time.deltaTime;
            if (_timeToNextAction < _timeSinceLastAction)
            {
                _timeSinceLastAction = 0;
                if (_isAttacking)
                {
                    _timeToNextAction = Random.value * 2f + 1f;
                    ArmIdle.SetVisibility(Globals.ALPHA_VISIBLE);
                    ArmAttack.SetVisibility(Globals.ALPHA_HIDDEN);
                    _isAttacking = false;
                }
                else
                {
                    _timeToNextAction = 0.3f;
                    ArmIdle.SetVisibility(Globals.ALPHA_HIDDEN);
                    ArmAttack.SetVisibility(Globals.ALPHA_VISIBLE);
                    PlayerManager.Instance.AttackPlayer();
                    _isAttacking = true;
                }
            }
        }
    }

    #endregion MonoBehaviour

    #region Public Methods

    /// <summary>
    /// Attacking the Skeleton and applying damage
    /// </summary>
    /// <returns>Returns if the skeleton is alive/the attack can be made</returns>
    public bool Attack(int dmg)
    {
        if (_currentHP > 0)
        {
            _currentHP -= dmg;
            if (_currentHP <= 0)
            {
                transform.gameObject.SetVisibility(Globals.ALPHA_HIDDEN);
                ArmIdle.SetVisibility(Globals.ALPHA_HIDDEN);
                ArmAttack.SetVisibility(Globals.ALPHA_HIDDEN);
                RoomManager.Instance.MonsterDeath();
            }
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// "Summon" a new skeleton ;)
    /// </summary>
    public void SetAlive()
    {
        _currentHP = InitialHP;
        _timeSinceLastAction = 0;
        _timeToNextAction = Random.value + 0.75f;
        transform.gameObject.SetVisibility(Globals.ALPHA_VISIBLE);
        ArmIdle.SetVisibility(Globals.ALPHA_VISIBLE);
    }

    #endregion Public Methods
}