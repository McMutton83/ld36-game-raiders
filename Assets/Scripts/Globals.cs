﻿using UnityEngine;

public static class Globals
{
    #region Constants

    public const float ALPHA_HIDDEN = 0.2f;
    public const float ALPHA_VISIBLE = 1f;

    #endregion Constants

    #region GameObject Extension

    public static void SetVisibility(this GameObject gameObject, float visibilty)
    {
        var renderer = gameObject.GetComponent<SpriteRenderer>();
        if (renderer != null)
        {
            Color itemColor = renderer.color;
            itemColor.a = visibilty;
            renderer.color = itemColor;
        }
    }

    #endregion GameObject Extension
}