﻿using UnityEngine;

/// <summary>
/// Handling Keyboard inputs and processing animations for sword/shield.
/// </summary>
public class PlayerManager : MonoBehaviour
{
    #region Fields

    private Direction _lookDirection;

    private float _currentWeaponCooldown;
    private float _timeWeaponRendered;
    private bool _isWeaponVisible;

    private float _currentShieldCooldown;
    private float _timeShieldRendered;
    private bool _isShieldVisible;

    private int _currentAttack;
    private int _currentLife;

    private float _timeGameOverShown;
    private bool _isGameOverVisible;

    #endregion Fields

    #region Properties

    public int InitialLife = 3;
    public int InitialAttack = 1;
    public GameObject[] Hearts;
    public GameObject[] Swords;
    public GameObject AttackLeft;
    public GameObject AttackRight;
    public GameObject Shield;
    public GameObject GameOver;
    public float WeaponCooldown = 1.5f;
    public float ShieldCooldown = 1.5f;
    public AudioSource PlayerHurt;
    private bool _gameWon;

    #endregion Properties

    public PlayerManager()
    {
        Instance = this;
    }

    #region Singleton

    public static PlayerManager Instance { get; set; }

    #endregion Singleton

    #region MonoBehaviour

    // Use this for initialization
    private void Start()
    {
        ResetPlayer();
    }

    private void HideAttackAndShield()
    {
        AttackLeft.SetVisibility(Globals.ALPHA_HIDDEN);
        AttackRight.SetVisibility(Globals.ALPHA_HIDDEN);
        Shield.SetVisibility(Globals.ALPHA_HIDDEN);
    }

    // Update is called once per frame
    private void Update()
    {
        ProcessAnimations();
        ProcessKeyboardInput();
    }

    #endregion MonoBehaviour

    public void WonGame()
    {
        _gameWon = true;
    }

    public void ItemReceived(LootType type)
    {
        switch (type)
        {
            case LootType.HEALTH:
                if (_currentLife < 5)
                {
                    _currentLife++;
                    OnLifeChanged();
                }
                break;

            case LootType.ATTACK_POWER:
                if (_currentLife < 5)
                {
                    _currentAttack++;
                    OnSwordsChanged();
                }
                break;
        }
    }

    public void AttackPlayer()
    {
        if (_currentLife > 0)
        {
            if (!_isShieldVisible)
            {
                PlayerHurt.Play();
                _currentLife -= 1;
                OnLifeChanged();
            }
        }
    }

    public void ResetPlayer()
    {
        _gameWon = false;
        GameOver.SetVisibility(Globals.ALPHA_HIDDEN);
        _lookDirection = Direction.NORTH;
        _currentAttack = InitialAttack;
        OnSwordsChanged();
        _currentLife = InitialLife;
        OnLifeChanged();
        HideAttackAndShield();
    }

    #region Private Methods

    #region Animation

    private void ProcessAnimations()
    {
        if (_currentLife > 0)
        {
            if (_isWeaponVisible)
            {
                _timeWeaponRendered += Time.deltaTime;
                if (_timeWeaponRendered > 0.2f)
                {
                    _isWeaponVisible = false;
                    AttackLeft.SetVisibility(Globals.ALPHA_HIDDEN);
                    AttackRight.SetVisibility(Globals.ALPHA_HIDDEN);
                    _timeWeaponRendered = 0;
                }
            }
            else
            {
                _currentWeaponCooldown -= Time.deltaTime;
            }
            if (_isShieldVisible)
            {
                _timeShieldRendered += Time.deltaTime;
                if (_timeShieldRendered > 1.2f)
                {
                    _isShieldVisible = false;
                    Shield.SetVisibility(Globals.ALPHA_HIDDEN);
                }
            }
            else
            {
                _currentShieldCooldown -= Time.deltaTime;
            }
        }
        else
        {
            _timeGameOverShown += Time.deltaTime;
            if (_isGameOverVisible)
            {
                if (_timeGameOverShown > 0.15f)
                {
                    _timeGameOverShown = 0;
                    GameOver.SetVisibility(Globals.ALPHA_HIDDEN);
                    _isGameOverVisible = false;
                }
            }
            else
            {
                if (_timeGameOverShown > 0.15f)
                {
                    _timeGameOverShown = 0;
                    GameOver.SetVisibility(Globals.ALPHA_VISIBLE);
                    _isGameOverVisible = true;
                }
            }
        }
    }

    #endregion Animation

    #region Keyboard Inputs

    private void ProcessKeyboardInput()
    {
        if (_currentLife > 0 && !_gameWon)
        {
            bool left = Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow);
            bool right = Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow);
            bool up = Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow);
            //bool down = Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow);

            bool primaryButton = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Y);
            bool secondaryButton = Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyDown(KeyCode.X);

            TurnAround(left, right);
            MoveForward(up);

            ProcessActionButtons(primaryButton, secondaryButton);
        }
    }

    private void ProcessActionButtons(bool primaryButton, bool secondaryButton)
    {
        if (primaryButton)
        {
            if (_currentWeaponCooldown <= 0)
            {
                Position pos = RoomManager.Instance.AttackSkeleton(_currentAttack);
                if (pos == Position.LEFT)
                {
                    AttackLeft.SetVisibility(Globals.ALPHA_VISIBLE);
                    _isWeaponVisible = true;
                }
                else if (pos == Position.RIGHT)
                {
                    AttackRight.SetVisibility(Globals.ALPHA_VISIBLE);
                    _isWeaponVisible = true;
                }
                _timeWeaponRendered = 0;
                _currentWeaponCooldown = WeaponCooldown;
            }
        }
        if (secondaryButton)
        {
            if (_currentShieldCooldown <= 0)
            {
                // If there isn't something other to do... which there isn't in this version. ^^
                // Examples could be opening doors, climb ladders, etc.
                if (!RoomManager.Instance.UseSecondaryAction())
                {
                    Shield.SetVisibility(Globals.ALPHA_VISIBLE);
                    _isShieldVisible = true;

                    _timeShieldRendered = 0;
                    _currentShieldCooldown = ShieldCooldown;
                }
            }
        }
    }

    private void MoveForward(bool up)
    {
        if (up)
        {
            RoomManager.Instance.MoveForward(_lookDirection);
        }
    }

    private void TurnAround(bool left, bool right)
    {
        if (left)
        {
            _lookDirection -= 1;
            RoomManager.Instance.TurnAround(_lookDirection);
        }
        else if (right)
        {
            _lookDirection += 1;
            RoomManager.Instance.TurnAround(_lookDirection);
        }
        if (_lookDirection < 0)
        {
            _lookDirection = 4 + _lookDirection;
        }
        _lookDirection = (Direction)(((int)_lookDirection) % 4);

        if (Debug.isDebugBuild)
        {
            Debug.Log(_lookDirection.ToString());
        }
    }

    #endregion Keyboard Inputs

    #region Status Updates

    private void OnSwordsChanged()
    {
        for (int i = 0; i < Swords.Length; i++)
        {
            var sword = Swords[i];
            if (i >= _currentAttack)
                sword.SetVisibility(Globals.ALPHA_HIDDEN);
            else
                sword.SetVisibility(Globals.ALPHA_VISIBLE);
        }
    }

    private void OnLifeChanged()
    {
        for (int i = 4; i >= 0; i--)
        {
            var heart = Hearts[i];
            if (i >= _currentLife)
                heart.SetVisibility(Globals.ALPHA_HIDDEN);
            else
                heart.SetVisibility(Globals.ALPHA_VISIBLE);
        }

        if (_currentLife == 0)
        {
            // GAME OVER!
        }
    }

    #endregion Status Updates

    #endregion Private Methods
}