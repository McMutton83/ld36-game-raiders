﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handling loot/drop chances
/// </summary>
public class LootManager : MonoBehaviour
{
    #region Fields

    //private List<GameObject> _registeredLoot;
    private Dictionary<GameObject, float> _registeredLoot;

    private GameObject _itemShown;

    #endregion Fields

    #region Singleton

    public static LootManager Instance { get; set; }

    #endregion Singleton

    #region Properties

    public GameObject FinalItem;
    private float _timeShown;

    #endregion Properties

    #region Constructor

    public LootManager()
    {
        Instance = this;
        _registeredLoot = new Dictionary<GameObject, float>();
    }

    #endregion Constructor

    #region MonoBehaviour

    // Use this for initialization
    private void Start()
    {
        Reset();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_itemShown != null)
        {
            _timeShown += Time.deltaTime;
            if (_timeShown > .75f)
            {
                _itemShown.SetVisibility(Globals.ALPHA_HIDDEN);
                _itemShown = null;
                _timeShown = 0;
            }
        }
    }

    #endregion MonoBehaviour

    #region Public Methods

    public void Reset()
    {
        if (FinalItem != null)
            FinalItem.SetVisibility(Globals.ALPHA_HIDDEN);
        if (_itemShown != null)
        {
            _itemShown.SetVisibility(Globals.ALPHA_HIDDEN);
        }
    }

    public LootType LootItem()
    {
        float totalChance = 0;
        foreach (var pair in _registeredLoot)
        {
            totalChance += pair.Value;
        }
        float rollOnLootTable = Random.value * totalChance;
        float sum = 0;
        LootItem lastItem = null;
        foreach (var pair in _registeredLoot)
        {
            lastItem = pair.Key.GetComponent<LootItem>();
            sum += pair.Value;
            if (sum >= rollOnLootTable)
            {
                _itemShown = pair.Key;
                _itemShown.SetVisibility(Globals.ALPHA_VISIBLE);
                break;
            }
        }

        // should be unreachable code.
        return lastItem.Type;
    }

    public void LootFinalItem()
    {
        FinalItem.SetVisibility(Globals.ALPHA_VISIBLE);
    }

    /// <summary>
    /// Registering and Hiding loot Gameobjects/Sprites
    /// </summary>
    public void RegisterLoot(GameObject lootItem, float dropChance = 0f)
    {
        lootItem.SetVisibility(Globals.ALPHA_HIDDEN);

        _registeredLoot.Add(lootItem, dropChance);
    }

    #endregion Public Methods
}